import bg from '../assets/bg.png';
import welcome from '../assets/welcome.png';
import findus from '../assets/findus.png';

export default {
  bg,
  welcome,
  findus,
};
