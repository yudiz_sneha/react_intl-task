import React from 'react';
import './AboutUs.css';
import Translate from '../../intl/Translate';

const AboutUs = () => (
  <div className="app__aboutus app__bg flex__center section__padding" id="about">

    <div className="app__aboutus-content flex__center">
      <div className="app__aboutus-content_about">
        <h1 className="headtext__cormorant">{Translate("aboutUs")}</h1>
        <p className="p__opensans">{Translate("homeLine")}</p>
      </div>
    </div>
  </div>
);

export default AboutUs;
