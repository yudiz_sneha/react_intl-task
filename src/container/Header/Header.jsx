import React from 'react';
import { images } from '../../constants';
import Translate from '../../intl/Translate';
import './Header.css';

const Header = () => (
  <div className="app__header app__wrapper section__padding" id="home">
    <div className="app__wrapper_info">
      <h1 className="app__header-h1">{Translate("homeTitle")}</h1>
      <p className="p__opensans" style={{ margin: '2rem 0' }}>{Translate("homeLine")}</p>
    </div>

    <div className="app__wrapper_img">
      <img src={images.welcome} alt="header_img" />
    </div>
  </div>
);

export default Header;
