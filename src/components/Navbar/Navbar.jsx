import React from 'react';
import { GiHamburgerMenu } from 'react-icons/gi';
import { MdOutlineRestaurantMenu } from 'react-icons/md';
import { useStateValue } from "../../state/StateProvider";
import './Navbar.css';
import Translate from "../../intl/Translate";

const Navbar = () => {
  const [{ lang }, dispatch] = useStateValue();

	const handleChange = (e) => {
		dispatch({
			type: "SET_LANG",
			lang: e.target.value,
		});
	};

  const [toggleMenu, setToggleMenu] = React.useState(false);
  return (
    <nav className="app__navbar">
      <div className="app__navbar-logo">
       <h1 style={{ color: 'white', fontFamily: 'cursive' }}>{Translate("homeRest")}</h1>
      </div>
      <ul className="app__navbar-links">
        <li className="p__opensans"><a href="#home">{Translate("navHome")}</a></li>
        <li className="p__opensans"><a href="#about">{Translate("navAbout")}</a></li>
        <div className="select" style={{ float: 'right' }}>
						<select onChange={handleChange}>
							<option value="en-us">English</option>
							<option value="de-de">German</option>
							<option value="fr-ca">French</option>
						</select>
					</div>
      </ul>

      <div className="app__navbar-smallscreen">
        <GiHamburgerMenu color="#fff" fontSize={27} onClick={() => setToggleMenu(true)} />
        {toggleMenu && (
          <div className="app__navbar-smallscreen_overlay flex__center slide-bottom">
            <MdOutlineRestaurantMenu fontSize={27} className="overlay__close" onClick={() => setToggleMenu(false)} />
            <ul className="app__navbar-smallscreen_links">
              <li><a href="#home" onClick={() => setToggleMenu(false)}>{Translate("navHome")}</a></li>
              <li><a href="#about" onClick={() => setToggleMenu(false)}>{Translate("navAbout")}</a></li>
            </ul>
          </div>
        )}
      </div>
    </nav>
  );
};

export default Navbar;
