import { LOCALES } from "../locales";
const data = {
	[LOCALES.ENGLISH]: {
		navHome: "Home",
		navAbout: "About",
		homeTitle: "The Key To Fine Dining",
		homeLine: "Grill N Chill Amazing Restaurant!!",
		homeRest: "Grill n Chill",
		aboutUs: "About Us",
	},
};
export default data;
