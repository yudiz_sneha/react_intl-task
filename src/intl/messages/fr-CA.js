import { LOCALES } from "../locales";
const data = {
	[LOCALES.FRENCH]: {
		navHome: "Maison",
		navAbout: "Sur",
		homeTitle: "La clé de la gastronomie",
		homeLine:
			"Griller et Chill Restaurant incroyable!!",
		homeRest: "Griller et Chill",
		aboutUs: "À propos de nous",
	},
};
export default data;