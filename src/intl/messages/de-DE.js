import { LOCALES } from "../locales";
const data = {
	[LOCALES.GERMAN]: {
		navHome: "Zuhause",
		navAbout: "Über",
		homeTitle: "Der Schlüssel zum feinen Essen",
		homeLine:
			"Grillen und Chillen Tolles Restaurant!!",
		homeRest: "Grillen und Chillen",
		aboutUs: "Über uns",
	},
};
export default data;
