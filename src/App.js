import React from 'react';
import { AboutUs, Header } from './container';
import { Navbar } from './components';
import { useStateValue } from "./state/StateProvider";
import { I18nProvider } from "./intl";
import './App.css';

function App() {
	const [{ lang }] = useStateValue();
	return (
		<div className="App">
		<I18nProvider locale={lang}>
          <Navbar />
          <Header />
		  <AboutUs />
		</I18nProvider>
		</div>
	);
}

export default App;
